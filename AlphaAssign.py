from plinkio import plinkfile
import numpy as np
import pedigree
import sys
import assignEvaluate

# Print statement for some clusters
import builtins as __builtin__
oldPrint = __builtin__.print
def print(*args, **kwargs):
    oldPrint(*args, **kwargs); sys.stdout.flush()
__builtin__.print = print

argDict = dict()
for arg in sys.argv:
    args = arg.split("=")
    if len(args) > 1:
        argDict[args[0]]=args[1]
    else:
        argDict[args[0]] = "True"
#Setup variables


def setFromArgs(arg, default=None, flagValue = None):
    if arg in argDict:
        if flagValue is not None: return flagValue
        return argDict[arg]
    return default


doFullOutput = setFromArgs("fullOutput", False, True)
genoError = float(setFromArgs("error", 0.01))
seqError = float(setFromArgs("seqError", 0.01))

bfile = setFromArgs("bfile")
aiFile = setFromArgs("file")
seqFile = setFromArgs("seqFile")
hapsFile = setFromArgs("hapsFile")

pedFile = setFromArgs("pedigree")

sireFile = setFromArgs("potentialSires")
damFile = setFromArgs("potentialDams")
outputFile = setFromArgs("out")

subset = setFromArgs("snplist")
# bfile = "example/genotypes"
# bfile = "data/genotypes.bin"
# aiFile = "data/AlphaImpute.genotypes"
# seqFile = "data/sequence.genotypes"

# bfile = "data/genotypes.firstGen.bin"
# seqFile = "data/sequence.lastGen.genotypes"
# aiFile = "data/AlphaImpute.partial.genotypes"

# sireFile = "sire.list"
# outputFile = "output.txt"

print("bfile", bfile)
print("file", aiFile)
print("hapsFile", hapsFile)
print("seqFile", seqFile)

if sireFile is None : "No sire file given."
if outputFile is None: "No output file given."


ped = pedigree.Pedigree()
if bfile is not None: pedigree.readInPlinkFile(ped, fileName=bfile, error = genoError)
if pedFile is not None: pedigree.readInPedigree(ped, pedFile)
if aiFile is not None: pedigree.readInAlphaImpute(ped, aiFile, error = genoError)
if seqFile is not None: pedigree.readInSequenceData(ped, seqFile, seqError = seqError)
if hapsFile is not None: pedigree.readInGenotypeProbabilities(ped, hapsFile)

if subset is not None:
    with open(subset, 'r') as f:
        lines = f.readlines()
    f.close()
    subsetMarkers = []
    for block in range(len(lines)) :
        parts = lines[block].split()
        subsetMarkers.append(parts[1])
    ped = ped.subset(subsetMarkers)


# Set up minor allele frequency
ped.useMaf=True
ped.setMaf()

# Run the sire and dam assignements

sireAssignements = None
damAssignments = None

if sireFile is not None:
    sireAssignements = assignEvaluate.readInAssignments(sireFile, findSire = True)
    for assignment in sireAssignements:
        assignment.runAssignment(ped, error = genoError)

if damFile is not None:
    damAssignments = assignEvaluate.readInAssignments(damFile, findSire = False)
    for assignment in damAssignments:
        assignment.runAssignment(ped, error = genoError)

if sireAssignements is not None: ped.updateBasedOnAssignments(sireAssignements, useTop = False)
if damAssignments is not None: ped.updateBasedOnAssignments(damAssignments, useTop = False)

ped.writePedigree(outputFile + ".pedigree")

if sireAssignements is not None: ped.updateBasedOnAssignments(sireAssignements, useTop = True)
if damAssignments is not None: ped.updateBasedOnAssignments(damAssignments, useTop = True)

ped.writePedigree(outputFile + ".pedigree.top")

if sireAssignements is not None:
    with open(outputFile + ".sires", 'w+') as f:
        f.write("id candidate score chosen estSire estFullSib estNull\n")
        for assignment in sireAssignements:
            f.write(assignment.writeSimpleLine())

if damAssignments is not None:
    with open(outputFile + ".dams", 'w+') as f:
        f.write("id candidate score chosen estSire estFullSib estNull\n")
        for assignment in damAssignments:
            f.write(assignment.writeSimpleLine())

if doFullOutput :
    if sireAssignements is not None:
        with open(outputFile + ".sires.full", 'w+') as f:
            for assignment in sireAssignements:
                f.write(assignment.writeLine())

    if damAssignments is not None:
        with open(outputFile + ".dams.full", 'w+') as f:
            for assignment in damAssignments:
                f.write(assignment.writeLine())


