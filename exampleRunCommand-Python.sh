

#The following code will run the pedigree assignemnt algorithm on "sampleSireList.txt", using genotype data from "genotypes.*".
#AlphaAssign requires pandas, numpy and plinkio. Use pip to install each package if missing.
#AlphaAssign is written for python 3.5 and above.

pip install -r requirements.txt
python AlphaAssign.py \
    bfile=example/genotypes \
    potentialSires=example/sampleSireList.txt \
    out=example/out.txt

cp example/out.txt .
