target=AlphaAssign-Python
mkdir $target

cp requirements.txt $target
cp pedigree.py $target
cp assignEvaluate.py $target
cp AlphaAssign.py $target
cp exampleRunCommand-Python.sh $target
cp AlphaAssignDocs/AlphaAssignDocs.pdf $target
cp -r example $target

zip -r $target


# ###
# ###
# ###

# mkdir ../AlphaAssign-Linux
# target=../AlphaAssign-Linux

# mkdir $target/binaries
# cp binaries/AlphaAssignLinux $target/binaries
# cp exampleRunCommand-Linux.sh $target
# cp AlphaAssignDocs/AlphaAssignDocs.pdf $target
# cp -r example $target

# zip -r ../AlphaAssign-Linux.zip $target


# ###
# ###
# ###

# mkdir ../AlphaAssign-Mac
# target=../AlphaAssign-Mac

# mkdir $target/binaries
# cp binaries/AlphaAssign-Mac $target/binaries
# cp exampleRunCommand-Linux.sh $target/exampleRunCommand-Mac.sh
# cp AlphaAssignDocs/AlphaAssignDocs.pdf $target
# cp -r example $target

# zip -r ../AlphaAssign-Mac.zip $target

