import numpy as np

def checkParentage(ped, idx) :
    if idx not in ped.pedigree :
        return (None, None)
    findSireId, damId = ped.pedigree[idx]
    baseValueWithoutfindSire = evaluateProbabilities(idx, None, damId, ped)
    baseValueWithoutDam = evaluateProbabilities(idx, findSireId, None, ped)
    value = evaluateProbabilities(idx, findSireId, damId, ped)

    if baseValueWithoutfindSire > value: findSireId = None
    if baseValueWithoutDam > value: damId = None
    return (findSireId, damId)

def createProbAssignement(ped, childId, parentList, alternativeParent=None, findSire = True, error = 0.01):
    scores = []
    baseValue = getBaseValues(childId, alternativeParent, findSire, ped, error = error)

    for parentId in parentList:
        if findSire:
            sireId = parentId
            damId = alternativeParent
        else:
            sireId = alternativeParent
            damId = parentId
        value = evaluateProbabilities(childId, sireId, damId, ped)
        scores.append(-value)
    bestIndex = np.argmin(scores)
    targetfindSire = parentList[bestIndex]

    putativeScore = scores[bestIndex]
    sireMean, sireSD = baseValue[0]
    fullMean, fullSD = baseValue[1]

    sireDist = abs(sireMean - putativeScore)/sireSD
    fullDist = abs(fullMean - putativeScore)/fullSD

    chosenId = None
    if putativeScore < sireMean or sireDist < fullDist : chosenId = targetfindSire
    
    return((chosenId, targetfindSire, baseValue, scores))


def evaluateProbabilities(childId, sireId = None, damId = None, ped=None) :
    childGenotypes = ped.getGenotypeProbabilities(childId).T
    findSireGenotypes = ped.getGenotypeProbabilities(sireId).T
    damGenotypes = ped.getGenotypeProbabilities(damId).T

    return(evaluateProbabilitiesNoPhase(childGenotypes, findSireGenotypes, damGenotypes, ped))

def getBaseValues(childId, alternativeParent, findSire, ped, error = 0.01) :
    childGenotypes = ped.getGenotypeProbabilities(childId).T
    alternativeGenotypes = ped.getGenotypeProbabilities(alternativeParent).T
    
    putativeSireGenotypes = peelUp(childGenotypes, alternativeGenotypes, ped, findSire = findSire)
    fullSibGenotypes = generateFullSib(putativeSireGenotypes, ped)
    halfSibGenotypes = generateHalfSib(putativeSireGenotypes, ped)
    nullGenotypes = ped.getGenotypeProbabilities(None).T

    trueScore = evauateExpAndVar(childGenotypes, putativeSireGenotypes, alternativeGenotypes, findSire, ped, error = error)
    fullSibScore = evauateExpAndVar(childGenotypes, fullSibGenotypes, alternativeGenotypes, findSire, ped, error = error)
    halfSibScore = evauateExpAndVar(childGenotypes, halfSibGenotypes, alternativeGenotypes, findSire, ped, error = error)
    nullScore = evauateExpAndVar(childGenotypes, nullGenotypes, alternativeGenotypes, findSire, ped, error = error)

    return((trueScore, fullSibScore, halfSibScore, nullScore))

def peelUp(childGenotypes, altGenotypes, ped, findSire) : #Will handle dam case seperately. Want to check this.
    if findSire:
        outerProductGenotypes = np.einsum("ai, ci -> aci", childGenotypes, altGenotypes)
        segregation = generateSegregation(partial=True)
        genotypeProbabilities = np.einsum("aci, abc -> bi", outerProductGenotypes, segregation)
    else:
        outerProductGenotypes = np.einsum("ai, bi -> abi", childGenotypes, altGenotypes)
        segregation = generateSegregation(partial=True)
        genotypeProbabilities = np.einsum("abi, abc -> ci", outerProductGenotypes, segregation)

    genotypeProbabilities *= ped.getGenotypeProbabilities(None).T
    normalizedGenotypes = genotypeProbabilities/np.sum(genotypeProbabilities, 0)
    return(normalizedGenotypes) #Maybe should include genotyping error.

def generateFullSib(indEstimate, ped) :

    segregation = generateSegregation(partial=True)

    parentEstimate = np.einsum("ai, abc -> bci", indEstimate, segregation)
    maf = ped.getGenotypeProbabilities(None).T
    parentEstimate = np.einsum("bci, bi, ci -> bci", parentEstimate, maf,maf)
    fullSibEstimate = np.einsum("bci, abc -> ai", parentEstimate, segregation)

    normalizedGenotypes = fullSibEstimate/np.sum(fullSibEstimate, 0)
    return(normalizedGenotypes)


def generateHalfSib(indEstimate, ped) :

    segregation = generateSegregation(partial=True)

    parentEstimate = np.einsum("ai, abc -> bi", indEstimate, segregation)
    maf = ped.getGenotypeProbabilities(None).T
    parentEstimate = np.einsum("bi, ci -> bci", parentEstimate, maf)
    halfSibEstimate = np.einsum("bci, abc -> ai", parentEstimate, segregation)

    normalizedGenotypes = halfSibEstimate/np.sum(halfSibEstimate, 0)
    return(normalizedGenotypes)



def evauateExpAndVar(childGenotypes, sireGenotypes, damGenotypes, findSire, ped, error = 0.01) :

    altScore = evaluateProbabilitiesNoPhase(childGenotypes, sireGenotypes, damGenotypes, ped)

    errorMat = np.array([[1-error*3/4, error/4, error/4, error/4], 
                            [error/4, .5-error/4, .5-error/4, error/4],
                            [error/4, .5-error/4, .5-error/4, error/4],
                            [error/4, error/4, error/4, 1-error*3/4]]).T



    segregation = generateSegregation(partial=True)
    collapsedGenotypes = np.einsum("ai, ci, be -> abcei", childGenotypes, damGenotypes, errorMat)
    likelihoods = np.einsum("abcei, abc -> ei", collapsedGenotypes, segregation)
    logLikelihoods = np.log(likelihoods)

    means = np.einsum("bi, bi -> i", logLikelihoods, sireGenotypes)
    meansSquared = np.einsum("bi, bi -> i", logLikelihoods**2, sireGenotypes)

    mean = np.sum(means)
    variance = np.sum(meansSquared - means**2)
    return (-mean, variance**.5)
    # (mean, variance, altScore)

def evaluateProbabilitiesNoPhase(childGenotypes, sireGenotypes, damGenotypes, ped) :

    outerProductGenotypes = np.einsum("ai, bi, ci -> abci", childGenotypes, sireGenotypes, damGenotypes)
    segregation = generateSegregation(partial=True)
    segregationLikelihoods = np.einsum("abci, abc -> i", outerProductGenotypes, segregation)
    logFactor = np.sum(np.log(segregationLikelihoods))
    return(logFactor)
   

def generateSegregation(partial=False) :
    paternalTransmission = np.array([ [1, 1, 0, 0],[0, 0, 1, 1]])
    maternalTransmission = np.array([ [1, 0, 1, 0],[0, 1, 0, 1]])

    fatherAlleleCoding = np.array([0, 0, 1, 1])
    motherAlleleCoding = np.array([0, 1, 0, 1])

    # !                  fm  fm  fm  fm 
    # !segregationOrder: pp, pm, mp, mm

    segregationTensor = np.zeros((4, 4, 4, 4))
    for segregation in range(4):
        if(segregation == 0) :
            father = paternalTransmission
            mother = paternalTransmission
        if(segregation == 1) :
            father = paternalTransmission
            mother = maternalTransmission
        if(segregation == 2) :
            father = maternalTransmission
            mother = paternalTransmission
        if(segregation == 3) :
            father = maternalTransmission
            mother = maternalTransmission

        # !alleles: aa, aA, Aa, AA
        for allele in range(4) :
            segregationTensor[allele, :, :, segregation] = np.outer(father[fatherAlleleCoding[allele]], mother[motherAlleleCoding[allele]])

    if partial : segregationTensor = np.mean(segregationTensor, 3)
    return(segregationTensor)


def readInAssignments(fileName, findSire) :
    assignments = []
    with open(fileName) as f:
        lines = f.readlines()
    for line in lines:
        parts = line.strip().split()
        idx = parts[0]
        potentialSires = parts[1:]
        potentialSires = [sire for sire in potentialSires if sire != 0]
        # print("%s %s" %(idx, potentialSires))
        assignments.append(AssignmentHolder(idx, potentialSires, findSire))
    return assignments



class AssignmentHolder(object):
    def __init__(self, idx, potentialSires, findSire = True):
        self.idx = idx
        self.findSire = findSire
        self.potentialParents = {idx:None for idx in potentialSires}
        self.baseScore = None
        self.chosenId = None
        self.topId = None

    def runAssignment(self, ped, error = 0.01):
        print(self.idx)
        alternativeParent = None
        if self.findSire and self.idx in ped.pedigree: alternativeParent = ped.pedigree[self.idx][1]
        if not self.findSire and self.idx in ped.pedigree: alternativeParent = ped.pedigree[self.idx][0]
        parentList = list(self.potentialParents.keys())
        newSire, topId, baseScore, scores = createProbAssignement(ped, self.idx, parentList, alternativeParent=alternativeParent, findSire = self.findSire, error = error)
        self.chosenId = newSire
        self.topId = topId
        self.baseScore = baseScore
        self.potentialParents = {}
        for index in range(len(parentList)) :
            self.potentialParents[parentList[index]] = scores[index]

    def writeLine(self) :
        chosenId = self.chosenId
        if self.chosenId is None: chosenId = 0
        sires = []
        scores = []
        for parent, score in self.potentialParents.items() :
            sires.append(parent)
            scores.append(score)
        line = "%s %s %s %s %s\n" %(self.idx, chosenId, self.baseScore, sires, scores)
        return line

    def writeSimpleLine(self) :
        chosenId = self.chosenId
        if self.chosenId is None: chosenId = 0
        sires = []
        scores = []
        line = "" 
        for parent, score in self.potentialParents.items() :
            sires.append(parent)
            scores.append(score)
            wasChosen = int(parent == chosenId)
            line += "%s %s %s %s %s %s %s\n" %(self.idx, parent, score, wasChosen, self.baseScore[0][0], self.baseScore[1][0], self.baseScore[3][0])
        return line



