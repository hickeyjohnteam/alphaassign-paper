\contentsline {section}{Introduction}{1}{section*.4}
\contentsline {subsection}{Availability}{1}{section*.5}
\contentsline {subsection}{Conditions of use}{1}{section*.6}
\contentsline {subsection}{Disclaimer}{2}{section*.7}
\contentsline {section}{Run commands and spec file}{2}{section*.8}
\contentsline {section}{Input file formats}{3}{section*.9}
\contentsline {subsection}{Genotype file}{3}{section*.10}
\contentsline {subsection}{AlphaGenes Genotype file}{3}{section*.11}
\contentsline {subsection}{Sequence read counts}{3}{section*.12}
\contentsline {subsection}{Haplotype file}{3}{section*.13}
\contentsline {subsection}{Pedigree file}{4}{section*.14}
\contentsline {subsection}{Potential sires file}{4}{section*.15}
\contentsline {section}{Output file formats}{4}{section*.16}
\contentsline {subsection}{Output file}{4}{section*.17}
\contentsline {subsection}{Full output file}{4}{section*.18}
