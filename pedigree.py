import numpy as np
import math

from plinkio import plinkfile

def readPlinkFile(fileName, startSnp=None, endSnp=None) :
    plinkFile = plinkfile.open( fileName )
    sampleList = plinkFile.get_samples( )
    locusList = plinkFile.get_loci( )
    numSnps = len(locusList)
    numInd = len(sampleList)

    if startSnp is None : 
        startSnp = 0
        endSnp = numSnps
    values = np.zeros((numInd, endSnp-startSnp))

    for snpIndex in range(startSnp-1):
        plinkFile.next()
    for snpIndex in range(endSnp-startSnp) :
        if snpIndex % 1000 == 0 : print("read %s out of %s" %(snpIndex, endSnp-startSnp))
        row = np.array(plinkFile.next())
        values[:,snpIndex] = np.array(row)
    print("Read finished")
    return values, sampleList, locusList

def readInPlinkFile(ped, fileName, error) :
    genotypes, sampleList, locusList = readPlinkFile(fileName)
    ped.markerLabels = [locus.name for locus in locusList]

    genotypes = genotypes.astype(np.int)

    idList = [sample.iid for sample in sampleList]
    pedigree = {sample.iid:(sample.father_iid, sample.mother_iid) for sample in sampleList}
    idToIndex = {sample.iid:index for index, sample in enumerate(sampleList)}

    ped.chromosomes = np.array([locus.chromosome for locus in locusList]).astype(np.int)
    ped.numChrom = np.max(ped.chromosomes)
    # genotypesToProbabilities = np.array([[1-error, error/2, error/2, error/2], 
    #                                     [error/2, 1-error, 1-error, error/2],
    #                                     [error/2, error/2, error/2, 1-error]])
    errorMat = np.array([[1-error*3/4, error/4, error/4, error/4], 
                            [error/4, .5-error/4, .5-error/4, error/4],
                            [error/4, error/4, error/4, 1-error*3/4], 
                            [.25, .25, .25, .25]])

    for idx in idList :
        sireId, damId = pedigree[idx]
        if sireId == "0": sireId = None
        if damId == "0": damId = None
        ped.pedigree[idx] =(sireId, damId) 
        vals = errorMat[genotypes[idToIndex[idx],:],:].T
        ped.genoProbs[idx] = vals/np.sum(vals,0)[None,:]
    ped.nloci = len(locusList)

def readInSequenceData(ped, fileName, seqError = .001) :
    log1 = math.log(1-seqError)
    log2 = math.log(.5)
    loge = math.log(seqError)

    with open(fileName) as f:
        lines = f.readlines()
    f.close()
    print("Reading in sequence:", fileName, "number of ind", len(lines)/2)
    for block in range(int(len(lines)/2)) :

        genotypes = []
        parts = lines[2*block].split(); idx = parts[0]; genotypes.append( np.array([float(val) for val in parts[1:]]))
        parts = lines[2*block+1].split(); idx = parts[0]; genotypes.append( np.array([float(val) for val in parts[1:]]))
        nloci = len(parts)-1

        vals = np.array([log1*genotypes[0] + loge*genotypes[1],
                            log2*genotypes[0] + log2*genotypes[1],
                            log2*genotypes[0] + log2*genotypes[1],
                            log1*genotypes[1] + loge*genotypes[0]])
        vals = np.exp(vals)
        ped.genoProbs[idx] = vals/np.sum(vals,0)[None,:] #Need to check this
        if idx not in ped.pedigree :
            ped.pedigree[idx] = (None, None)

    print("Finished reading in haplotypes")
    ped.setNLoci(nloci)

def readInAlphaImpute(ped, fileName, error) :
    # genotypesToProbabilities = np.array([[1-error, error/2, error/2, error/2], 
    #                                         [error/2, 1-error, 1-error, error/2],
    #                                         [error/2, error/2, error/2, 1-error]])
    errorMat = np.array([[1-error*3/4, error/4, error/4, error/4], 
                            [error/4, .5-error/4, .5-error/4, error/4],
                            [error/4, error/4, error/4, 1-error*3/4],
                            [.25, .25, .25, .25]])

    with open(fileName) as f:
        lines = f.readlines()
    f.close()

    print("Reading in AlphaImpute Format:", fileName, "number of lines", len(lines))
    for block in range(len(lines)) :
        parts = lines[block].split(); idx = parts[0]; genotypes=np.array([int(val) for val in parts[1:]])
        nloci = len(parts)-1
        vals = errorMat[genotypes,:].T
        ped.genoProbs[idx] = vals/np.sum(vals,0)[None,:] 
        if idx not in ped.pedigree :
            ped.pedigree[idx] = (None, None)

    ped.setNLoci(nloci)

def readInPedigree(ped, fileName):
    with open(fileName) as f:
        lines = f.readlines()
    f.close()

    for block in range(len(lines)) :
        parts = lines[block].split()
        idx = parts[0]
        # if not( idx not in ped.pedigree or ped.pedigree[idx] == (None, None)):
        #     print("Replacing pedigree entry for ", idx, "from pedigree")
        if parts[1] == "0": parts[1] = None
        if parts[2] == "0": parts[2] = None
        ped.pedigree[parts[0]] = (parts[1], parts[2])

def readInGenotypeProbabilities(ped, fileName) :
    nloci = ped.nloci
    with open(fileName) as f:
        lines = f.readlines()
    f.close()

    print("Reading in haplotypes:", fileName, "number of ind", len(lines)/4)
    for block in range(int(len(lines)/4)) :
        genotypes = []
        parts = lines[4*block].split(); idx = parts[0]; genotypes.append( np.array([float(val) for val in parts[1:]]))
        parts = lines[4*block+1].split(); idx = parts[0]; genotypes.append( np.array([float(val) for val in parts[1:]]))
        parts = lines[4*block+2].split(); idx = parts[0]; genotypes.append( np.array([float(val) for val in parts[1:]]))
        parts = lines[4*block+3].split(); idx = parts[0]; genotypes.append( np.array([float(val) for val in parts[1:]]))
        nloci = len(parts)-1

        ped.genoProbs[idx] = np.array([genotypes[0], genotypes[1], genotypes[2], genotypes[3]])
        if idx not in ped.pedigree :
            ped.pedigree[idx] = (None, None)

    print("Finished reading in haplotypes")

    ped.setNLoci(nloci)


class Pedigree(object):
 
    def __init__(self, nloci = None, pedigree = None, genoProbs = None):
        self.numChrom = None
        self.chromosomes = None

        self.markerLabels = None

        self.genoProbs = dict()
        self.pedigree = dict()
        self.useMaf = False
        self.maf = None
        self.nloci = 0
         
        if nloci is not None: self.nloci = nloci
        if pedigree is not None: self.pedigree = pedigree
        if genoProbs is not None: self.genoProbs = genoProbs


    def subset(self, markerList) :

        which = []
        if self.markerLabels is None:
            print("Marker labels required. Use plink formatted input")

        for index in range(len(self.markerLabels)):
            marker = self.markerLabels[index]
            if marker in markerList :
                which.append(index)
        subset = np.array(which, dtype=np.int64)
        newPed = Pedigree(nloci = len(subset), pedigree = self.pedigree)
        newPed.numChrom = self.numChrom
        newPed.chromosomes = self.chromosomes[subset]
        newPed.genoProbs = {idx:self.genoProbs[idx][:,subset] for idx in self.genoProbs}
        newPed.markerLabels = [self.markerLabels[index] for index in which]

        newPed.useMaf = self.useMaf
        if self.maf is not None:
            newPed.maf = self.maf[subset]

        return(newPed)

    def getGenotypeOfId(self, idx):
        return(self.genotypes[self.idToIndex[idx],:])
    
    def setMaf(self) :
        toDosages = np.array([2,1,1,0])[:,None]/2 #Maf is minor not major allele.
        weights = 2*np.ones(self.nloci)
        dosages = np.ones(self.nloci)

        for idx, prob in self.genoProbs.items():
            dosages += np.sum(toDosages*prob, 0) #check this is reasonable.
            weights += np.sum(prob, 0) #check this is reasonable.
        self.maf = dosages/weights
    def setNLoci(self, nloci):
        if nloci != self.nloci and self.nloci >0 :
            print("Observed ", nloci, "loci however initially had, ", self.nloci)
        if self.nloci == 0: self.nloci = nloci

    def getGenotypeProbabilities(self, idx, usePhase = False):
        if idx in self.genoProbs.keys():
            genoProbs = self.genoProbs[idx].T.copy()
            if not usePhase :
                genoProbs[:,1:3] = np.sum(genoProbs[:,1:3],1)[:,None]/2
                return(genoProbs)
            return(genoProbs)
        elif idx is None:
            pass
        else:
            print("No genotype information found for", idx, "returning empty genotypes")

        if self.useMaf and self.maf is not None:
            maf = self.maf
            return(np.array([maf**2, maf*(1-maf), (1-maf)*maf, (1-maf)**2]).T)

        return(np.tile(np.array([.25, .25, .25,.25]), (self.nloci,1))) #Choose this or MAF. To do
    def writePedigree(self, fileName):
        file = open(fileName, 'w+')

        for idx in self.pedigree:
            sireId, damId = self.pedigree[idx]
            if sireId is None: sireId = "0"
            if damId is None: damId = "0"
            file.writelines("%s %s %s\n" %(idx, sireId, damId))
        file.close()
    def updateBasedOnAssignments(self, assignmentList, useTop = False):
        for assignment in assignmentList:
            idx = assignment.idx
            curSire, curDam = self.pedigree[idx]
            newId = assignment.chosenId
            if useTop: newId = assignment.topId

            if assignment.findSire :
                self.pedigree[idx] = (newId, curDam)
            else:
                self.pedigree[idx] = (curSire, newId)


